# BGPaaS Operator for Contrail with Kubernetes

This operator facilitates the creation, update, and deletion of BGPaaS resources in conjunction with OpenStack Machines in a Kubernetes cluster deployed with CAPO+CAPO. When an OpenStack machine is instantiated, the operator will dynamically generate and manage corresponding BGPaaS resources leveraging Juniper's Contrail system.

## Overview

- Operator identifies OpenStack machine creations in the cluster.
- A corresponding HeatStack resource is generated for each OpenStack machine.
- Each HeatStack resource will have BGPaaS configurations.
- Operator continues to monitor OpenStack machines and updates or deletes the corresponding HeatStacks as necessary.

**References**:
- [Contrail - Configuring BGP as a Service](https://supportportal.juniper.net/s/article/Contrail-Example-Configuring-BGP-as-a-Service?language=en_US)
- [BGP-as-a-Service Overview in Contrail](https://www.juniper.net/documentation/en_US/contrail4.1/topics/concept/bgp-as-a-service-overview.html)

## Prerequisites

- A running Kubernetes cluster.
- Juniper's Contrail system configured and accessible.
- Helm for deploying the operator.

## Configuration

Configuration is primarily driven through environment variables. Here are the available configurations:

- **NAMESPACE**: Kubernetes namespace in which the operator runs. Default is `default`.
- **API_VERSION_HEATSTACK**: API version for HeatStacks.
- **DEFAULT_PORT**: Default values for port name termination in `<domain_name>:<project_name>:<virtual_machine_name>-<port_name_termination>` format. Used for creating BGPaaS resource.
- **DEFAULT_ASN**: Autonomous System Number for BGP.
- **BGP_ADDRESS_FAMILIES**: Address families for BGP. Can be 'inet' or 'inet, inet6' (defaults to 'inet')

## Deployment

1. Clone the Helm chart repository.

2. Update the `values.yaml` as required:

   ```yaml
   conf:
     env:
       - name: NAMESPACE
         value: your-namespace
       ...
   ```

3. Install the operator:

   ```bash
   helm install bgpaas-operator ./path-to-helm-chart
   ```

4. Verify that the operator is running:

   ```bash
   kubectl get pods -n your-namespace
   ```

## Usage

Once deployed, the operator will automatically detect and process OpenStack machines. For every OpenStack machine that gets created in the specified namespace, a HeatStack with BGPaaS resources will be generated.

For any OpenStack machine labeled with `cluster.x-k8s.io/control-plane`, a corresponding HeatStack is created or updated. On deletion of an OpenStack machine, the corresponding HeatStack is also removed.

## Debugging

Logs of the operator can provide valuable information:

```bash
kubectl logs bgpaas-operator-pod-name -n your-namespace
```

## Contributing & Support

For issues, please raise them in the respective repository. Contributions are welcome through merge requests.

---

This README provides a basic guide to the purpose and usage of the BGPaaS Operator. You might need to adjust some of the steps, links, and configurations based on your specific environment and infrastructure setup.