#!/usr/bin/env python
import time
import logging
from kubernetes import client, config
from kubernetes.client.rest import ApiException
from kubernetes.client import CoreV1Api
import kopf
import uuid
import os
import base64
import yaml

# Constants
API_GROUP_OSM = 'infrastructure.cluster.x-k8s.io'
API_GROUP_HEATSTACK = 'heatoperator.sylva'
PLURAL_OPENSTACKMACHINE = 'openstackmachines'
PLURAL_HEATSTACK = 'heatstacks'

API_VERSION_HEATSTACK = os.environ.get('API_VERSION_HEATSTACK', 'v1')
DEFAULT_PORT = os.environ.get('DEFAULT_PORT', '0')
DEFAULT_ASN = os.environ.get('DEFAULT_ASN', '')
if os.environ.get('ADDRESS_FAMILIES'):
  BGP_ADDRESS_FAMILIES = os.environ.get('ADDRESS_FAMILIES').split(',')
else:
  BGP_ADDRESS_FAMILIES = ['inet', 'inet6']



# Configure logging
logging.basicConfig(level=logging.INFO)

# Configure Kubernetes client
config.load_incluster_config()
api_instance = client.CustomObjectsApi()
core_v1_api = CoreV1Api()

# Configure logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


# Function to get various data from the body
def process_body_data(body):
    annotations = body.get('metadata', {}).get('annotations', {})
    metadata = body.get('metadata', {})
    name = metadata['name']
    labels = metadata.get('labels', {})
    namespace = metadata.get('namespace', '')
    heatstack_identity_ref = body.get('spec', {}).get('identityRef', {}).get('name', '')
    cloud_name = body.get('spec', {}).get('identityRef', {}).get('cloudName', '')
    kube_secret_file = core_v1_api.read_namespaced_secret(heatstack_identity_ref, namespace)
    decoded_data = {k: base64.b64decode(v).decode('utf-8') for k, v in kube_secret_file.data.items()}
    parsed_yaml_data = yaml.safe_load(decoded_data.get('clouds.yaml', '{}'))
    cloud_data = parsed_yaml_data.get('clouds', {}).get(cloud_name, {}).get('auth', {})

    required_cloud_keys = ['project_domain_name', 'project_name']
    
    for key in required_cloud_keys:
        if key not in cloud_data:
            raise Exception(f"no '{key}' in clouds.yaml clouds.{cloud_name}.auth")
     
    _domain_name = cloud_data.get('project_domain_name')
    domain_name = "default-domain" if _domain_name == "Default" else _domain_name
    project_name = cloud_data.get('project_name', '')
    
    return annotations, metadata, name, labels, namespace, heatstack_identity_ref, domain_name, project_name, cloud_name


def create_heatstack_object(body):
    bgpaas_resource_name = f"BGPaaS-{str(uuid.uuid4())[:8]}"  # Generate a random name for BGPaaS resource
    annotations, metadata, machine_name, labels, namespace, heatstack_identity_ref, domain_name, project_name, cloud_name = process_body_data(body)
    port_name = annotations.get('sylva.capo-contrail-bgpaas.port-name', DEFAULT_PORT)
    asn = annotations.get('sylva.capo-contrail-bgpaas.asn', DEFAULT_ASN)

    heatstack = {
        "apiVersion": f"{API_GROUP_HEATSTACK}/{API_VERSION_HEATSTACK}",
        "kind": "HeatStack",
        "metadata": {
            "name": machine_name,
            "namespace": namespace,
            "labels": 
                { 
                    "type": "sylva.capo-contrail-bgpaas"
                },
            "ownerReferences": [
                {
                    "apiVersion": f"{API_GROUP_OSM}/{API_VERSION_HEATSTACK}",
                    "kind": "OpenStackMachine",
                    "name": metadata['name'],
                    "uid": metadata['uid'],
                    "controller": True,
                    "blockOwnerDeletion": True
                }
            ]
        },
        "spec": {
            "outputConfigMap": {
                "name": f"capo-contrail-resources-{machine_name}"
            },
            "identityRef": heatstack_identity_ref,
            "cloudName": cloud_name,
            "heatStack": {
                 "environment": {
                    "parameters": {}
                },
                "template": {
                    "heat_template_version": str("2018-08-31"),
                    "resources": {
                        bgpaas_resource_name: {
                            "properties": {
                                "autonomous_system": asn,
                                "bgpaas_session_attributes": {
                                    "bgpaas_session_attributes_address_families": {
                                        "bgpaas_session_attributes_address_families_family": BGP_ADDRESS_FAMILIES
                                    }
                                },
                                "name": bgpaas_resource_name,
                                "virtual_machine_interface_refs": [f'{domain_name}:{project_name}:{machine_name}-{port_name}']
                            },
                            "type": "OS::ContrailV2::BgpAsAService"
                        }
                    }
                }
            }
        }
    }

    return heatstack

# Helper function to create heatstack
def create_heatstack(body):
    _, _, machine_name, _, namespace, _, _, _, _ = process_body_data(body)
    heatstack = create_heatstack_object(body)
    try:
        api_instance.get_namespaced_custom_object(
            group=API_GROUP_HEATSTACK,
            version=API_VERSION_HEATSTACK,
            namespace=namespace,
            plural=PLURAL_HEATSTACK,
            name=machine_name,
        )
    except ApiException as e:
        if e.status == 404:
            # If the HeatStack doesn't exist, create it
            logging.info("Create heatstack :")
            logging.info(heatstack)
            api_instance.create_namespaced_custom_object(
                group=API_GROUP_HEATSTACK,
                version=API_VERSION_HEATSTACK,
                namespace=namespace,
                plural=PLURAL_HEATSTACK,
                body=heatstack,
            )
        else:
            logging.error(f"An ApiException occurred with status {e.status}. Aborting create_heatstack.")
            return

## Helper function to reconcile heatstack
def reconcile_heatstack(body):
    heatstack = None
    _, _, metadata_name, labels, namespace, _, _, _, _ = process_body_data(body)
    try:
        heatstack = api_instance.get_namespaced_custom_object(
            group=API_GROUP_HEATSTACK,
            version=API_VERSION_HEATSTACK,
            namespace=namespace,
            plural=PLURAL_HEATSTACK,
            name=metadata_name,
        )
        logging.info(f"Retrieved HeatStack resource with status: {heatstack.get('status', {})}")
    except ApiException as e:
        if e.status == 404 and 'cluster.x-k8s.io/control-plane' in labels:   # //  Check if we need to add extra label for control plane nodes of workload clusters //
            logging.info("Reconciling... Creating heatstack")
            create_heatstack(body)
        else:
            logging.error(f"ApiException occurred with status {e.status}. Aborting reconcile_heatstack.")
            return
    except Exception as e:
        logging.critical(f"An unexpected critical error occurred in reconcile_heatstack: {e}")
        return

    if heatstack and 'status' in heatstack:
        create_heatstack_flag = True  # Assume that HeatStack needs to be created
        for condition in heatstack['status'].get('conditions', []):
            if condition.get('reason', '') == 'CREATE_COMPLETE':
                create_heatstack_flag = False  # HeatStack exists and is complete, no need to create
                break

        if create_heatstack_flag:
            logging.info(f"Heatstack status is not CREATE_COMPLETE for the following resource: {metadata_name}")
            create_heatstack(body)

# Handler function to create heatstack
@kopf.on.create(PLURAL_OPENSTACKMACHINE)
def handle_create(body, **kwargs):
    # Extract necessary data from body
    _, metadata, name, labels, _, _, _, _, _  = process_body_data(body)

    logger.info(f"Creating resource :" + name)
    # Skip if OpenStackMachine is not for K8s control-plane
    if 'cluster.x-k8s.io/control-plane' not in labels:  # // Check if we need to add extra label for control plane nodes of workload clusters //
        logger.info(f"Machine is not part of management cluster, not removing " + name)
        return

    # Create heatstack
    create_heatstack(body)
    # Update the status
    kopf.event(body, type='Normal', reason='HeatstackReconciled', message='Heatstack Created successfully.')
    # Start reconciliation timer
    return {'status': 'created'}

# Timer handler
@kopf.timer(PLURAL_OPENSTACKMACHINE, id=lambda name, **_: f'timer-{name}', interval=30, idle=30)
def handle_timer(body, **kwargs):
    _, _, name, labels, namespace, _, _, _, _  = process_body_data(body)
    if 'cluster.x-k8s.io/control-plane' in labels:
        logger.info(f"Timer triggered for :" + name)
        reconcile_heatstack(body)
        kopf.event(body, type='Normal', reason='HeatstackReconciled', message='Heatstack reconciled successfully.')


# Handler function to delete heatstack
@kopf.on.delete(PLURAL_OPENSTACKMACHINE)
def handle_delete(body, **kwargs):
    _, _, name, labels, namespace, _, _, _, _  = process_body_data(body)

    logger.info(f"Deleting resource :" + name)

    if 'cluster.x-k8s.io/control-plane' not in labels:    #  // Check if we need to add extra label for control plane nodes of workload clusters //
        logger.info(f"Not deleting :" + name)
        return

    # Check if heatstack exists before deleting
    try:
        api_instance.get_namespaced_custom_object(
            group=API_GROUP_HEATSTACK,
            version=API_VERSION_HEATSTACK,
            namespace=namespace,
            plural=PLURAL_HEATSTACK,
            name=name
        )
    except ApiException as e:
        if e.status == 404:  # Heatstack not found, nothing to do
            logging.warning(f"Heatstack with name {name} doesn't exist. Nothing to delete.")
            return
        else:  # Some other exception occurred, re-raise it
            logging.error(f"An ApiException occurred while getting the heatstack object: {e}")
            raise


    # Delete heatstack
    try:
        logging.info("Deleting heatstack")
        api_instance.delete_namespaced_custom_object(
            group=API_GROUP_HEATSTACK,
            version=API_VERSION_HEATSTACK,
            namespace=namespace,
            plural=PLURAL_HEATSTACK,
            name=name,
            body=client.V1DeleteOptions(),
        )
        kopf.event(body, type='Normal', reason='HeatstackReconciled', message='Heatstack successfully deleted.')
    except ApiException as e:
        logging.error(f"An ApiException occurred while deleting the heatstack object: {e}")
        return

    # Return a dict with a status key that is then used by handle_timer
    return {'status': 'deleted'}

# Reconciliation for existing resources
def reconcile_existing_openstackmachines():
    try:
        openstackmachines = api_instance.list_cluster_custom_object(
            group=API_GROUP_OSM,
            version=API_VERSION_HEATSTACK,
            plural=PLURAL_OPENSTACKMACHINE,
        )
        logging.info(openstackmachines)
    except ApiException as e:
        # Handle API exceptions here
        logging.critical(f"An ApiException occurred while reconciling existing OpenStack machines: {e}")
        return

    for machine in openstackmachines.get('items', []):
        logger.info(machine)
        handle_create(machine)

if __name__ == '__main__':
    logger.info("Starting operator...")
    logger.info("Start reconcile existing machines...")
    reconcile_existing_openstackmachines()
    kopf.run(clusterwide=True)
